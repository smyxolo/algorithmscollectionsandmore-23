package Zadania3listopada17;

import java.util.ArrayList;
import java.util.List;

public class MyInsertSort {
    public static void main(String[] args) {
        List<Integer> tab = new ArrayList<>();

        tab.add(2);
        tab.add(4);
        tab.add(9);
        tab.add(1);
        tab.add(5);
        tab.add(3);
        tab.add(6);

        for (int i = 1; i < tab.size(); i++) {
            int klucz = tab.get(i);
            int j = i - 1;
            while (j >= 0 && tab.get(j) > klucz){
                tab.set(j + 1, tab.get(j));
                j = j - 1;
                tab.set(j + 1, klucz);
            }
        }

        System.out.println(tab);
    }
}
