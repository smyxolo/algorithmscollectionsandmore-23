Utwórz klasę Produkt i Magazyn. Produkt ma mieć cenę, ilość oraz nazwę. Magazym ma mieć tablicę produktów. W funkcji main użyj sortowania, aby posortować produkty. Nie pobieraj z klasy Magazyn całej tablicy, tylko utwórz metodę/metody pobierające pojedynczy produkt.
Posortuj elementy po:
a) ilości
b) nazwie
Użyj własnej implementacji sortowania.