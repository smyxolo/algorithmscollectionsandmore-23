package Zadania_31pazdziernik17.Zadanie57;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna: ");
        int liczba = skan.nextInt();

        System.out.println("Kwadrat liczby " + liczba + " to: " + kwadrat(liczba));


    }

    private static int kwadrat(int liczba){
        int kwadrat = 0;
        for (int i = 1; i <= liczba * 2; i++) {
            if(i % 2 == 1) kwadrat += i;
        }
        return kwadrat;
    }
}
