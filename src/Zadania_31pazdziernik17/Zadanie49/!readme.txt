Napisz program kopiujący pliki, których nazwy zostały podane jako parametry wywołania.
W metodzie main pobierz od użytkownika dwie ścieżki do plików (*łatwe, przypisz na sztywno ścieżkę do plików), a następnie przekaż te ścieżki do metody:
kopiuj (String sourcePath, String targetPath)
Metoda ta tworzy nowe obiekty File{Input,Outptu}Stream, a następnie kopiuje bit po bicie dane z jednego strumienia do drugiego. Jeżeli pliki nie istnieją, wyświetl taką informację dla użytkownika.