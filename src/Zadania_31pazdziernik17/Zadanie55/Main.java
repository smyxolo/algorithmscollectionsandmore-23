package Zadania_31pazdziernik17.Zadanie55;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        int liczba1 = 0;
        int liczba2 = 0;

        System.out.println("Podaj dwie liczby: ");
        String[] inputArgs = skan.nextLine().split(" ", 2);

        try {
            liczba1 = Integer.parseInt(inputArgs[0]);
            liczba2 = Integer.parseInt(inputArgs[1]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        System.out.println("Najmniejsza wspólna wielokrotność to: " + nww(liczba1, liczba2));



    }

    private static int nww(int liczba1, int liczba2){
        int nww = liczba1 * liczba2;
        for (int i = 1; i <= liczba1 * liczba2; i++) {
            if (i % liczba1 == 0 && i % liczba2 == 0) {
                nww = i;
                break;
            }
        }
        return nww;
    }
}
