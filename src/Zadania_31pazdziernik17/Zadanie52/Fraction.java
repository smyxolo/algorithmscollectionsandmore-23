package Zadania_31pazdziernik17.Zadanie52;

public class Fraction {

    //licznik i mianownik
    private int numerator;
    private int denominator;

    //konstruktor uwzgledniajacy ujemne wartosci licznika i mianownika
    public Fraction(int numerator, int denominator) {
        if ((numerator < 0 && denominator < 0)) {
            this.numerator = Math.abs(numerator);
            this.denominator = Math.abs(denominator);
        } else if ((numerator > 0 && denominator < 0)) {
            this.numerator = -numerator;
            this.denominator = Math.abs(denominator);
        } else {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    //konstruktor dla liczby calkowitej
    public Fraction(int numerator) {
        this.numerator = numerator;
        this.denominator = 1;
    }

    //konstruktor bezparametrowy dla "0"
    public Fraction() {
        this.numerator = 0;
        this.denominator = 1;
    }

    //konstruktor kopiujacy
    public Fraction(Fraction fraction){
        this.numerator = fraction.numerator;
        this.denominator = fraction.denominator;
    }


    //popraw ulamek uwzgledniajac znaki licznika i mianownika
    public void correction(){
        if ((numerator < 0 && denominator < 0)) {
            numerator = Math.abs(numerator);
            denominator = Math.abs(denominator);
        } else if ((numerator > 0 && denominator < 0)) {
            numerator = -numerator;
            denominator = Math.abs(denominator);
        }
    }


    //zredukuj ulamek o najwiekszy wspolny dzielnik
    public void reduce(){
        int gcd = greatestCommonDivisor();
        if (gcd != 1){
            numerator = numerator / gcd;
            denominator = denominator / gcd;
        }
        else System.out.println("Fraction cannot be reduced.");
    }

    //oblicz najwiekszy wspolny dzielnik
    public Integer greatestCommonDivisor(){
        int gcd =1;
        int iterator = Math.min(numerator, denominator);
        for (int i = iterator; i >= 1; i--) {
            if(numerator % i == 0 && denominator % i == 0){
                gcd = i;
                break;
            }
        }
        return gcd;
    }

    //zredukuj ulamek o podana liczbe
    public void reduce(int number){
        if(numerator % number == 0 && denominator % number == 0){
            numerator /= Math.abs(number);
            denominator /= Math.abs(number);
        }
    }

    public void expand(int number){
        numerator *= number;
        denominator *= number;
    }

    //pomnoz ulamek przez obiekt ulamek
    public Fraction product(Fraction fractionMultiplier){
        Fraction toreturn = new Fraction(numerator * fractionMultiplier.getNumerator(),
                denominator * fractionMultiplier.getDenominator());
        return toreturn;
    }

    //pomnoz ulamek przez liczbe calkowita
    public Fraction product(int numberMultiplier){
        Fraction toreturn = new Fraction(numerator*numberMultiplier, denominator);
        toreturn.reduce();
        return toreturn;
    }

    //zamień ułamek na jego odwrotność
    public void multInv(){
        int temp = numerator;
        numerator = denominator;
        denominator = temp;
    }

    //zwraca odwrotność ułamka w parametrze
    public static Fraction multInv(Fraction fraction){
        return new Fraction(fraction.getDenominator(), fraction.getNumerator());
    }

    //zwraca wynik dzielenia ułamka przez ułamek podany w parametrze
    public Fraction div(Fraction fract){
        Fraction divisor = new Fraction(fract);
        divisor.multInv();
        return new Fraction(divisor.numerator * numerator, divisor.denominator * denominator);
    }

    //zwraca wynik dzielenia ułamka przez liczbę całkowitą w parametrze
    public Fraction div(int divisor){
        return new Fraction(numerator, denominator * divisor);
    }

    @Override
    public String toString() {
        if (denominator != 1) {
            return numerator +
                    "/" + denominator;
        } else {
            return ""+ numerator;
        }
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }
}
