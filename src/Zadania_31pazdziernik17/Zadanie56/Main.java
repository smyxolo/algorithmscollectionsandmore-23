package Zadania_31pazdziernik17.Zadanie56;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        int liczba1 = 6;
        int liczba2 = 15;

        System.out.println("Podaj dwie liczby: ");
        String[] inputArgs = skan.nextLine().split(" ", 2);

        try {
            liczba1 = Integer.parseInt(inputArgs[0]);
            liczba2 = Integer.parseInt(inputArgs[1]);
        } catch (NumberFormatException nfe) {
            System.out.println("Nieprawidłowy format liczby");
        } catch (ArrayIndexOutOfBoundsException aioobe){
            System.out.println("Podano zbyt mało elementów.");
        }

        System.out.println(nwd(liczba1, liczba2));

    }

    private static int nwd(int liczba1, int liczba2){

        int d1 = Math.min(liczba1, liczba2);
        int d2 = Math.max(liczba1, liczba2) - Math.min(liczba1, liczba2);

        while (d1 != d2){
            int d1b = Math.min(d1, d2);
            int d2b = Math.max(d1, d2) - Math.min(d1, d2);
            d1 = d1b;
            d2 = d2b;
        }
        return d1;
    }
}
