package ZadaniaAlgorytmy.Zadanie1;

public class TabliczkaMnozenia {
    public static void main(String[] args) {
        for (int i = 1; i <= 15; i++) {
            System.out.print(String.format("|%3d|", i));
            for (int j = 1; j <= 15 ; j++) {
                int value = i*j;
                System.out.printf("%3d|", value);
            }
            System.out.println();
        }
    }
}
