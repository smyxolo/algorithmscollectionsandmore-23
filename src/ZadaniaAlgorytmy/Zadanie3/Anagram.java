package ZadaniaAlgorytmy.Zadanie3;

import java.util.ArrayList;

public class Anagram {
    public static void main(String[] args) {

        String string1 = "rescued";
        String string2 = "reduced";
        String string3 = "seducer";

        System.out.println(string1 + " and "+ string2 + " are anagrams: " + isAnagram(string1, string2));
        System.out.println(string1 + " and "+ string3 + " are anagrams: " + isAnagram(string1, string3));
        System.out.println(string2 + " and "+ string3 + " are anagrams: " + isAnagram(string2, string3));

    }

    private static boolean isAnagram(String string1, String string2) {
        int counter  = 0;
        if (string1.length() == string2.length()){
            char[] s1CharArray = string1.toCharArray();
            char[] s2CharArray = string2.toCharArray();
            ArrayList<Character> compList = new ArrayList<>();
            for (char c1 : s1CharArray) {
                compList.add(c1);
            }
            for (char c2: s2CharArray) {
                for (int i = 0; i < compList.size(); i++) {
                    if(compList.get(i).equals(c2)){
                        compList.remove(compList.get(i));
                        break;
                    }
                }
            }
            if(compList.size() == 0){
                return true;
            }
            else return false;
        }
        else return false;
    }
}
