Pewna firma planuje wprowadzić nowy produkt na rynek. Chciałaby, żeby reklama tego produktu
jak najlepiej była dopasowana do odbiorców. Dlatego też zebrali imiona i nazwiska
osób, które kupowały ich produkty. Postanowili w reklamie umieścić najpopularniejsze
imiona i nazwiska. Otrzymaliśmy zadanie przygotowania programu, który obliczy
ile razy dane imię i nazwisko pojawia się w liście. Przy kopiowaniu wkradły się błędy.
Część liter jest z dużej litery. Oczywiście nasz program powinien poradzić sobie z tym.
Wyświetlamy dane w formacie (imię i nazwisko z dużej litery - reszta małymi):
Imię, ilość wystąpień
Imię, ilość wystąpień
Imię, ilość wystąpień

Nazwisko, ilość wystąpień
Nazwisko, ilość wystąpień

maLINOWSka julIA
KowAlSka zuZAnna
MaliNoWSka maja
malINOWska zuZANna
NoWAKOWska julIA
KowAlSka hANNa
NoWAKOWska zOFia
KowAlSka lena
Nowacka maja
WOJTKowska aMelIA
MaliNoWSka zofia
WojTKOWSka zuZAnnA
Nowacka hANna
KowAlSka ameLIA
WoJTKowska ALIcja
KowAlSka ZOfIA
Nowakowska maJA
MaliNoWSka alEKSAndRA
KowAlSka alICJA
WojtkoWSKA julIA
NoWACka alEKSandra