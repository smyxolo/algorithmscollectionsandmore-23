package Zadania_26pazdziernik17.Zadanie3Filmy;

public class Miasto {
    private String nazwa;
    private int populacja;

    public Miasto(String nazwa, int populacja) {
        this.nazwa = nazwa;
        this.populacja = populacja;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getPopulacja() {
        return populacja;
    }

    public void setPopulacja(int populacja) {
        this.populacja = populacja;
    }

    @Override
    public String toString() {
        return "" + nazwa;
    }
}
