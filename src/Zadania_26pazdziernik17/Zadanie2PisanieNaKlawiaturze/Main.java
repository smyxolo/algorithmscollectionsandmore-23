package Zadania_26pazdziernik17.Zadanie2PisanieNaKlawiaturze;

//Przygotuj program, który będzie uczył ludzi szybszego pisania na klawiaturze. Program ma działać w pętli.
//        Pętla przestaje działać kiedy użytkownikowi uda się opróżnić kolejkę.
//        Na początku program wypełnia kolejkę 10 losowo wygenerowanymi napisami (o długości 15). Następnie co 10 sekund program dodaje kolejny wyraz. Jeżeli w kolejce będzie
//        40 wyrazów wyświetlamy przegraną i kończymy program. Użytkownik wpisuje po kolei napisy oddzielone enterami.
//        Gdy użytkownik wciśnie enter sprawdzamy czy napis wpisany przez użytkownika był dodany jako pierwszy (czy jest najstarszy).
//        Jeżeli tak usuwamy go z kolejki. Jeżeli nie czekamy na kolejny napis.

import java.util.*;

public class Main {
    private static List<String> wordQueue = fillQueue();
    private static Scanner skan = new Scanner(System.in);

    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new growList(), 0, 10000);
        while (stillPlaying()){
            System.out.println(wordQueue);
            System.out.println("first word: ");
            tryRemoval(skan.nextLine());
        }
        timer.cancel();
        if(wordQueue.size() == 0) {
            System.out.println("You won!");
        }
        else {
            System.out.println("You lost.");
        }
    }

    private static void tryRemoval(String inputLine){
        if(wordQueue.get(0).equals(inputLine)){
            wordQueue.remove(0);
        }
        else System.out.println("Incorrect word or inaccurate copy.");
    }

    private static String generateWord(){
        Random random = new Random();
        String alphabet = "";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            builder.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }
        return builder.toString();
    }

    private static List<String> fillQueue(){
        List<String> queue = new LinkedList<>();
        for (int i = 0; i < 7; i++) {
            queue.add(generateWord());
        }
        return queue;
    }

    private static class growList extends TimerTask{
        @Override
        public void run() {
            if(wordQueue.size() < 15) {
                wordQueue.add(generateWord());
            }
        }
    }

    private static boolean stillPlaying(){
        if (wordQueue.size() == 0 || wordQueue.size() >= 15) return false;
        else return true;
    }
}
