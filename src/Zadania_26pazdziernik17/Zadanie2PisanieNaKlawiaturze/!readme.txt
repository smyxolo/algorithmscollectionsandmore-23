Przygotuj program, który będzie uczył ludzi szybszego pisania na klawiaturze. Program ma działać w pętli.
Pętla przestaje działać kiedy użytkownikowi uda się opróżnić kolejkę.
Na początku program wypełnia kolejkę 10 losowo wygenerowanymi napisami (o długości 15). Następnie co 10 sekund program dodaje kolejny wyraz. Jeżeli w kolejce będzie
40 wyrazów wyświetlamy przegraną i kończymy program. Użytkownik wpisuje po kolei napisy oddzielone enterami.
Gdy użytkownik wciśnie enter sprawdzamy czy napis wpisany przez użytkownika był dodany jako pierwszy (czy jest najstarszy).
Jeżeli tak usuwamy go z kolejki. Jeżeli nie czekamy na kolejny napis.
