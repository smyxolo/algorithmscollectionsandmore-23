1. Coraz większą popularność wśród mło zyskuje stara gra w słówka.  Polega ona na tym, że pierwszy gracz mówi wybrane przez siebie słowo a gracz drugi musi podać słowo rozpoczynające się na literę ostatnią ze słowa pierwszego gracza.
Posiadając listę różnych wyrazów (inną dla każdego z graczy), sprawdź który z graczy (pierwszy czy drugi) wygra pojedynek w słówka.
Wybierz losowo pierwszą literę ze wszytkich wyrazów gracza 1. Następnie sprawdź kto wygra.
DODATKOWO: Wyświetl wyrazy jakie powi gracz 1 oraz gracz 2

Lista wyrazów gracza 1:
łuk
jabłko
jeden
chodnik
deska
sukienka
stary
terrarium
miłość
sypialnia
hotel
planety
wujek
huśtawka
obsługa
sok
grill
salon
obudowa
statek
grabie
kalosze
huśtawka
terrarium
noktowizor
zeszyt
pakuły
wędka
jeden
mama

Lista wyrazów gracza 2:
kot
brat
koszula
parapet
drzewo
terrarium
helikopter
kran
sypialnia
kawa
nóż
smok
stołówka
obudowa
siedemset
terrarium
poduszka
grunt
dom
rododendron
rzepa
młody
rolnik
zły
deska
porcelana
dwa
miłość
autostrada
słońce