Posługując się ArrayList'ą (jako polem klasy) zaimplementuj klasę która zachowuje się jak kolejka i która posiada metody:
    Stwórz strukturę FIFO która posiada metody:
        - push_back - dodawanie wartości (na koniec)
        - pop_front - wyciąganie wartości (wartość jest zwrócona) (z początku)
        - peek - zwraca następną wartość ale jej nie usuwa (podgląda)
        - size - zwraca rozmiar struktury
        - push_front - dodanie na początek
        - pop_back - wyciągnięcie z konca kolejki


Posługując się ArrayList'ą (jako polem klasy) zaimplementuj klasę MyPriorityQueue (bedzie Ci potrzebna klasa QueueElement), ktora posiada metody:
    - push - dodanie do kolejki
    - pop - wyciagniecie z kolejki
    - size - zwraca wielkosc kolejki
    - print - wypisuje elementy kolejki
*Kolejka powinna sortować elementy malejąco lub rosnąco w zależności od parametry konstruktora.
Add Comment