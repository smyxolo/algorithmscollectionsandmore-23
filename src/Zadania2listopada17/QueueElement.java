package Zadania2listopada17;

public class QueueElement<T> {
    private int priority;
    private T object;

    public QueueElement(int priority, T object) {
        this.priority = priority;
        this.object = object;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "QueueElement{" +
                "priority=" + priority +
                ", object=" + object +
                '}';
    }
}
