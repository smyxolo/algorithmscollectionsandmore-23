Stosując zaimplementuj aplikację, która monitoruje terminy przydatności produktów.
Aplikacja powinna obsługiwać komendy. Dopuszczalne komendy to:
    - dodaj nazwa cena ilosc [termin przydatności]
    - usun nazwa
    - sprawdz

- komenda dodaj: powinna do magazynu dodawać produkt o podanej nazwie.
                Jeśli produkt o takiej nazwie istnieje to operacja dodaj powinna AKTUALIZOWAĆ CENĘ tego produktu oraz dodawać ilość produktu.
                Dodatkowe[trudniejsze]*: Pod każdą nazwą produktu powinna istnieć informacja ile mamy produktów starej ważności
                Podstawowe[łatwe]: Po dodaniu ilości produktów i aktualizacji ceny aktualizuj również termin przydatności. (tak, system jest głupi
                                    i zamiast posiadać stare produkty ze starą datą przydatności o nowe produkty z nową, to system posiada tylko
                                    sumę obu ilości produktów z tą datą przydatności którą wpisaliśmy ostatnio)

            czyli:
                --------------------------------------------------
                Podstawowe:
                    po komendzie
                            dodaj 2.50 30 23.10.2017-18:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00

                    po komendzie
                            dodaj masło 3.0 10 03.11.2017-19:00

                    mam produkt:
                            masło 3.00 40 03.11.2017-19:00
                --------------------------------------------------
                Dodatkowe:
                    po komendzie
                            dodaj 2.50 30 23.10.2017-18:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00

                    po komendzie
                            dodaj masło 3.0 10 03.11.2017-19:00

                    mam produkt:
                            masło 2.50 30 23.10.2017-18:00
                            masło 3.00 10 03.11.2017-19:00

- komenda usun: usuwa [wszystkie] produkty o podanej nazwie.
- komenda sprawdz: wypisuje wszystkie produkty w kolejności od tych ktore sa najblizej przeterminowania, do tych ktore sa najdalej do przeterminowania.
                    Przy kazdej pozycji przeterminowanej powinna sie dopisac wzmianka o tym ze sa przeterminowane.

                    Mając produkty:
                            masło 2.50 30 20.10.2017-18:00
                            masło 3.00 10 03.11.2017-19:00
                            chleb 3.00 5  10.10.2017-18:00
                            wódka 10.00 1 03.11.2017-19:00

                        i wiedząc że jest dzien 25.10.2017 powinineś wypisać:
                            masło:
                                    - 2.50 30 20.10.2017-18:00 - przeterminowane
                                    - 3.00 10 03.11.2017-19:00
                            chleb
                                    - 3.00 5  10.10.2017-18:00 - przeterminowane
                            wódka
                                    - 10.00 1 03.11.2017-19:00



Dodatkowe*: spróbuj użyć kolejki priorytetowej do przechowywania produktów